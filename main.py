import os

g = 0
S = "_"
M = "_"
L = "_"
O = " "
U = " "
ende = False
claimed = False
hinweis = ""

def Normalwerte():
    global g # Kennzeichnung notwendig, da sonst eine lokale Variable g neben der globalen Variablen g entsteht
    global S
    global M
    global L
    global O
    global U
    global claimed 
    g = 0
    S = "_"
    M = "_"
    L = "_"
    O = " "
    U = " "
    claimed = False
    
def Ausgabe():
    print("#---------------#   ")
    print("| COFFEE to go  |   Preisliste:")
    print("|               |     S: 1 EUR")
    print("| select:       |     M: 2 EUR")
    print("| S", S, "M", M, "L", L, "  |     L: 3 EUR")
    print("|               |")
    print("| coin   claim  |", hinweis)
    print("| slot   |", O, "|  |")
    if(g == 0):
        print("| <   >  |", U, "|  |")
    else:
        print("| <", g, ">  |", U, "|  |")
    print("#---------------#")

Ausgabe()
print()
while(ende == False):
    if(claimed == True): 
        Normalwerte();
    print("Geben Sie eines der folgenden Zeichen ein und drücken Sie Enter!")
    print(" 1 / 2 Eine 1- oder 2-Euro-Muenze einwerfen")
    print(" S/M/L Kaffee Small, Medium oder Large wählen (nach Einwurf)")
    print(" E Programmende (Kein Rueckgeld!)")
    x = input("")
    if(x == "1"): 
        g = g + 1
    if(x == "2"): 
        g = g + 2
    if(x == "s" or x == "S"):
        if(g>=1):
            S = "x"
            U = "u"
            g = 0
            claimed = True
    if(x == "m" or x == "M"):
        if(g>=2):
            M = "x"
            U = "U"
            g = 0
            claimed = True
    if(x == "l" or x == "L"):
        if(g>=3):
            L = "x"
            O = "*"
            U = "U"
            g = 0
            claimed = True
    if(x == "e" or x == "E"):
        ende = True

    #clear screen on windows and macos and linux:
    os.system('cls' if os.name == 'nt' else 'clear')
    Ausgabe()
    print()